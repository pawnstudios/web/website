/*
 * Copyright (c) 2023  Alex (Pawn Studios)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


export async function onRequestGet(context) {
    const newRequest = new Request('https://discordapp.com/api/servers/193377420643008513/widget.json');

    let discordResponse = await fetch(newRequest, {
        cf: {
            cacheTtl: 300,
            cacheEverything: true,
        },
    });

    let functionResponse = new Response(discordResponse.body, discordResponse);
    functionResponse.headers.set("Cache-Control", "max-age=305");
    functionResponse.headers.delete("set-cookie");
    return functionResponse;
}

