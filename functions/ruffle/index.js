/*
 * Copyright (c) 2024  Alex (Pawn Studios)
 *
 * This Source Code Form, except for textual content is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

export async function onRequestGet(context) {
    const newRequest = new Request('https://unpkg.com/@ruffle-rs/ruffle');

    let ruffleResponse = await fetch(newRequest, {
        cf: {
            cacheTtl: 36000,
            cacheEverything: true,
        },
    });

    let functionResponse = new Response(ruffleResponse.body, ruffleResponse);
    functionResponse.headers.set("Cache-Control", "max-age=36005");
    functionResponse.headers.delete("set-cookie");
    return functionResponse;
}

